<?php

namespace App\Form;

use App\Entity\OpenHour;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OpenHourType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('day', ChoiceType::class, [
                'label' => 'openHour.form.day',
                'choices' => [
                    'dayOfWeek.sun' => 0,
                    'dayOfWeek.mon' => 1,
                    'dayOfWeek.tue' => 2,
                    'dayOfWeek.wed' => 3,
                    'dayOfWeek.thu' => 4,
                    'dayOfWeek.fri' => 5,
                    'dayOfWeek.sat' => 6,
                ],
            ])
            ->add('startTime', TimeType::class, [
                'label' => 'openHour.form.startTime',
                'input' => 'datetime',
                'widget' => 'single_text',
            ])
            ->add('endTime', TimeType::class, [
                'label' => 'openHour.form.endTime',
                'input' => 'datetime',
                'widget' => 'single_text',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OpenHour::class,
        ]);
    }
}
