<?php

namespace App\Form\Provider;

use App\Entity\Plan;
use App\Entity\OpenHour;
use App\Entity\Provider;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProviderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('companyName', TextType::class, [
                'label' => 'provider_ea.show.companyName',
                'attr' => [
                    'placeholder' => 'provider_ea.form.companyName',
                    'class' => 'input-piola',
                ],
            ])
            ->add('cif', NumberType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'provider_ea.form.cif',
                    'class' => 'input-piola',
                ],
            ])
            ->add('billingAddress', TextType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'provider_ea.form.billingAddress',
                    'class' => 'input-piola',
                ],
            ])
            ->add('deliveryTime', TextType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'provider_ea.form.deliveryTime',
                    'class' => 'input-piola',
                ],
            ])
            ->add('phone', TelType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'provider_ea.form.phone',
                    'class' => 'input-piola',
                ],
            ])
            ->add('mobile', TelType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'provider_ea.form.mobile',
                    'class' => 'input-piola',
                ],
            ])
            ->add('email', EmailType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'provider_ea.form.email',
                    'class' => 'input-piola',
                ],
            ])
            ->add('enabled', CheckboxType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'provider_ea.form.email',
                    'class' => 'input-piola',
                ],
            ])
            ->add('openHours', EntityType::class, [
                'class' => OpenHour::class,
                'label' => 'provider_ea.form.openHour',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.id', 'ASC');
                },
                'attr' => [
                    'class' => 'input-piola',
                ],
            ])
            ->add('plan', EntityType::class, [
                'class' => Plan::class,
                'label' => 'provider_ea.form.plan',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.id', 'ASC');
                },
                'attr' => [
                    'class' => 'input-piola',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Provider::class,
        ]);
    }
}
