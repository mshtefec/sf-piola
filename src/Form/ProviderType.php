<?php

namespace App\Form;

use App\Entity\Plan;
use App\Entity\Provider;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProviderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('companyName', TextType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'provider_ea.form.companyName',
                    'class' => 'form-control form-control-lg',
                ],
            ])
            ->add('cif', NumberType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'provider_ea.form.cif',
                    'class' => 'form-control form-control-lg',
                ],
            ])
            ->add('billingAddress', TextType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'provider_ea.form.billingAddress',
                    'class' => 'form-control form-control-lg',
                ],
            ])
            ->add('phone', TelType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'provider_ea.form.phone',
                    'class' => 'form-control form-control-lg',
                ],
            ])
            ->add('email', EmailType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'provider_ea.form.email',
                    'class' => 'form-control form-control-lg',
                ],
            ])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_options' => [
                    'attr' => [
                        'placeholder' => 'password',
                        'class' => 'form-control form-control-lg',
                    ],
                ],
                'second_options' => [
                    'attr' => [
                        'placeholder' => 'password_confirmation',
                        'class' => 'form-control form-control-lg',
                    ],
                ],
            ])
            ->add('plan', EntityType::class, [
                'class' => Plan::class,
                'label' => 'provider_ea.form.plan',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.id', 'ASC');
                },
                'attr' => [
                    'class' => 'form-control form-control-lg',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Provider::class,
        ]);
    }
}
