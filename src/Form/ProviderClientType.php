<?php

namespace App\Form;

use App\Entity\ProviderClient;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProviderClientType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('provider')
            ->add('discount')
            ->add('paymentMethod', ChoiceType::class,
            [
                'choices' => [
                    'Credit Card' => 1,
                    'On Delivery' => 2,
                    'Credit' => 3,
                ],
            ])
            ->add('delinquent', ChoiceType::class,
                [
                    'choices' => [
                        'Without incident' => 1,
                        'Debtor' => 2,
                        'Defaulter' => 3,
                        'Not sell' => 4,
                    ],
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProviderClient::class,
        ]);
    }
}
