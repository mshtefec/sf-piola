<?php

namespace App\Form;

use App\Entity\FormPlanEnterprise;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FormPlanEnterpriseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $choiceLanguage = $options['choice_language'];

        $builder
            ->add('country', null, [
                'label' => 'provider_ea.form.country',
                'attr' => [
                    'data-widget' => 'select2',
                ],
            ])
            ->add('language', ChoiceType::class, [
                'choices' => $choiceLanguage,
                'label' => 'provider_ea.form.language',
            ])
            ->add('companySize', ChoiceType::class, [
                'label' => 'provider_ea.form.companySize',
                'choices' => [
                    '1 a 15' => '1 a 15',
                    '15 a 25' => '15 a 25',
                    '25 a 50' => '25 a 50',
                    '+ de 50' => '+ de 50',
                ],
            ])
            ->add('amountUserStarted', ChoiceType::class, [
                'label' => 'provider_ea.form.amountUserStarted',
                'choices' => [
                    '1 a 10' => '1 a 10',
                    '10 a 20' => '10 a 20',
                    '20 a 30' => '20 a 30',
                    '30 a 50' => '30 a 50',
                    '+ de 50' => '+ de 50',
                ],
            ])
            ->add('amountReferenceStarted', null, [
                'label' => 'provider_ea.form.amountReferenceStarted',
            ])
            ->add('observation', null, [
                'label' => 'provider_ea.form.observation',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FormPlanEnterprise::class,
            'choice_language' => null,
        ]);
    }
}
