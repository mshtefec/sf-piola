<?php

namespace App\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PlanType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'choice_attr' => function ($choice, $key, $value) {
                return [
                    'data-completeFormPlanEnterprise' => ($choice->getCompleteFormPlanEnterprise()) ? '1' : '0',
                ];
            },
        ]);
    }

    public function getParent()
    {
        return EntityType::class;
    }
}
