<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ProviderConstraintValidator extends ConstraintValidator
{
    private $validator;
    private $translator;

    public function __construct(ValidatorInterface $validator, TranslatorInterface $translator)
    {
        $this->validator = $validator;
        $this->translator = $translator;
    }

    public function validate($value, Constraint $constraint)
    {
        if (
            $value->getPlan()->getCompleteFormPlanEnterprise() ||
            (
                $value->getFormPlanEnterprise() &&
                !is_null($value->getFormPlanEnterprise()->getId())
            )
        ) {
            $errors = $this->validator->validate($value->getFormPlanEnterprise());
            if (count($errors) > 0) {
                $this->context->buildViolation($this->translator->trans('verify Form plan enterprise'))
                    ->addViolation();
                foreach ($errors as $error) {
                    $this->context->buildViolation($error->getMessage())
                        ->atPath(sprintf('formPlanEnterprise.%s', $error->getPropertyPath()))
                        ->addViolation();
                }
            }
        }
    }
}
