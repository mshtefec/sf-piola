<?php

namespace App\Service;

class PlanService
{
    private $provider = null;
    private $isNew;

    public function configProviderPlan($provider, $planChange = true, $planExtend = false)
    {
        $this->provider = $provider;
        $this->isNew = is_null($provider->getId());
        $this->configForm();

        if ($planChange || $planExtend) {
            $this->setEndDatePlan($planExtend);
        }
    }

    public function configForm()
    {
        if ($this->isNew) {
            if (!$this->provider->getPlan()->getCompleteFormPlanEnterprise()) {
                $this->provider->setFormPlanEnterprise(null);
            }
        } elseif (!$this->provider->getPlan()->getCompleteFormPlanEnterprise()
                    && !$this->provider->getFormPlanEnterprise()->getId()) {
            $this->provider->setFormPlanEnterprise(null);
        }
    }

    public function setEndDatePlan($planExtend)
    {
        if (0 == $this->provider->getPlan()->getEnableDay()) {
            $this->provider->setIsPlanExtend(false);
            $this->provider->setEndDatePlan(null);
        } else {
            $dateExpired = new \DateTime();
            if (!$planExtend) {
                $day = $this->provider->getPlan()->getEnableDay();
            } else {
                $day = $this->provider->getPlan()->getExtendDays();
            }

            $this->provider->setEndDatePlan($dateExpired->modify(sprintf('+%u day', $day)));
        }
    }
}
