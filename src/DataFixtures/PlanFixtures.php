<?php

namespace App\DataFixtures;

use App\Entity\Plan;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class PlanFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $plan = new Plan();
        $plan->setName('Plan Prueba');
        $plan->setEnableDay(15);
        $plan->setExtendDays(15);
        $plan->setNumberUsers(0);
        $plan->setNumberReferences(0);
        $plan->setCompleteFormPlanEnterprise(false);
        $plan->setCreatedAt(new \DateTime());

        $manager->persist($plan);

        $plan = new Plan();
        $plan->setName('Plan Starter');
        $plan->setEnableDay(0);
        $plan->setNumberUsers(5);
        $plan->setNumberReferences(200);
        $plan->setCompleteFormPlanEnterprise(false);
        $plan->setCreatedAt(new \DateTime());

        $manager->persist($plan);

        $plan = new Plan();
        $plan->setName('Plan Pro');
        $plan->setEnableDay(0);
        $plan->setNumberUsers(10);
        $plan->setNumberReferences(500);
        $plan->setCompleteFormPlanEnterprise(false);
        $plan->setCreatedAt(new \DateTime());

        $manager->persist($plan);

        $plan = new Plan();
        $plan->setName('Plan Enterprise');
        $plan->setEnableDay(0);
        $plan->setNumberUsers(0);
        $plan->setNumberReferences(0);
        $plan->setCompleteFormPlanEnterprise(true);
        $plan->setCreatedAt(new \DateTime());

        $manager->persist($plan);

        $manager->flush();
    }
}
