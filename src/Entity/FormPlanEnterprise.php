<?php

namespace App\Entity;

use App\Entity\Traits\LanguageTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FormPlanEnterpriseRepository")
 */
class FormPlanEnterprise
{
    use LanguageTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=2, options={"default": "es"})
     * @Assert\NotNull
     */
    private $language = 'es';

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank
     */
    private $companySize;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank
     */
    private $amountUserStarted;

    /**
     * @ORM\Column(type="integer", options={"default" : 0})
     * @Assert\NotBlank
     */
    private $amountReferenceStarted = 0;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $observation;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Provider", mappedBy="formPlanEnterprise", cascade={"persist", "remove"})
     * @Assert\Valid
     */
    private $provider;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Country")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull
     */
    private $country;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLanguage(): ?string
    {
        return $this->language;
    }

    public function setLanguage(?string $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getCompanySize(): ?string
    {
        return $this->companySize;
    }

    public function setCompanySize(string $companySize): self
    {
        $this->companySize = $companySize;

        return $this;
    }

    public function getAmountUserStarted(): ?string
    {
        return $this->amountUserStarted;
    }

    public function setAmountUserStarted(string $amountUserStarted): self
    {
        $this->amountUserStarted = $amountUserStarted;

        return $this;
    }

    public function getAmountReferenceStarted(): ?int
    {
        return $this->amountReferenceStarted;
    }

    public function setAmountReferenceStarted(?int $amountReferenceStarted): self
    {
        $this->amountReferenceStarted = $amountReferenceStarted;

        return $this;
    }

    public function getObservation(): ?string
    {
        return $this->observation;
    }

    public function setObservation(?string $observation): self
    {
        $this->observation = $observation;

        return $this;
    }

    public function getProvider(): ?Provider
    {
        return $this->provider;
    }

    public function setProvider(?Provider $provider): self
    {
        $this->provider = $provider;

        // set (or unset) the owning side of the relation if necessary
        $newFormPlanEnterprise = null === $provider ? null : $this;
        if ($provider->getFormPlanEnterprise() !== $newFormPlanEnterprise) {
            $provider->setFormPlanEnterprise($newFormPlanEnterprise);
        }

        return $this;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }
}
