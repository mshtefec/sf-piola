<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Traits\ClientProviderTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\ClientRepository")
 * @UniqueEntity("email", entityClass="App\Entity\User")
 */
class Client extends User
{
    use ClientProviderTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OpenHour", mappedBy="client", cascade={"persist"})
     * @Assert\Valid
     */
    private $openHours;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProviderClient", mappedBy="client", orphanRemoval=true, cascade={"persist"})
     * @Assert\Valid
     */
    private $providerClients;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Address", mappedBy="client", cascade={"persist"})
     * @Assert\Valid
     */
    private $addresses;

    public function __construct()
    {
        $this->openHours = new ArrayCollection();
        $this->providerClients = new ArrayCollection();
        $this->addresses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|OpenHour[]
     */
    public function getOpenHours(): Collection
    {
        return $this->openHours;
    }

    public function addOpenHour(OpenHour $openHour): self
    {
        if (!$this->openHours->contains($openHour)) {
            $this->openHours[] = $openHour;
            $openHour->setClient($this);
        }

        return $this;
    }

    public function removeOpenHour(OpenHour $openHour): self
    {
        if ($this->openHours->contains($openHour)) {
            $this->openHours->removeElement($openHour);
            // set the owning side to null (unless already changed)
            if ($openHour->getClient() === $this) {
                $openHour->setClient(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ProviderClient[]
     */
    public function getProviderClients(): Collection
    {
        return $this->providerClients;
    }

    public function addProviderClient(ProviderClient $providerClient): self
    {
        if (!$this->providerClients->contains($providerClient)) {
            $this->providerClients[] = $providerClient;
            $providerClient->setClient($this);
        }

        return $this;
    }

    public function removeProviderClient(ProviderClient $providerClient): self
    {
        if ($this->providerClients->contains($providerClient)) {
            $this->providerClients->removeElement($providerClient);
            // set the owning side to null (unless already changed)
            if ($providerClient->getClient() === $this) {
                $providerClient->setClient(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Address[]
     */
    public function getAddresses(): Collection
    {
        return $this->addresses;
    }

    public function addAddress(Address $address): self
    {
        if (!$this->addresses->contains($address)) {
            $this->addresses[] = $address;
            $address->setClient($this);
        }

        return $this;
    }

    public function removeAddress(Address $address): self
    {
        if ($this->addresses->contains($address)) {
            $this->addresses->removeElement($address);
            // set the owning side to null (unless already changed)
            if ($address->getClient() === $this) {
                $address->setClient(null);
            }
        }

        return $this;
    }
}
