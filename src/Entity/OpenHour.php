<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\OpenHourRepository")
 */
class OpenHour
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     */
    private $day;

    /**
     * @ORM\Column(type="time", nullable=true)
     * @Assert\NotNull
     */
    private $startTime;

    /**
     * @ORM\Column(type="time", nullable=true)
     * @Assert\NotNull
     */
    private $endTime;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Provider", inversedBy="openHours")
     * @ORM\JoinColumn(nullable=true)
     * @Assert\Valid
     */
    private $provider;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Client", inversedBy="openHours")
     * @ORM\JoinColumn(nullable=true)
     * @Assert\Valid
     */
    private $client;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDay(): ?int
    {
        return $this->day;
    }

    public function setDay(int $day): self
    {
        $this->day = $day;

        return $this;
    }

    public function getStartTime(): ?\DateTimeInterface
    {
        return $this->startTime;
    }

    public function setStartTime(?\DateTimeInterface $startTime): self
    {
        $this->startTime = $startTime;

        return $this;
    }

    public function getEndTime(): ?\DateTimeInterface
    {
        return $this->endTime;
    }

    public function setEndTime(?\DateTimeInterface $endTime): self
    {
        $this->endTime = $endTime;

        return $this;
    }

    public function getProvider(): ?Provider
    {
        return $this->provider;
    }

    public function setProvider(?Provider $provider): self
    {
        $this->provider = $provider;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }
}
