<?php

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

trait ClientProviderTrait
{
    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     */
    private $cif;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $companyName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $billingAddress;

    /**
     * @ORM\Column(type="bigint")
     * @Assert\NotBlank
     */
    private $phone;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $mobile;

    public function getCif(): ?int
    {
        return $this->cif;
    }

    public function setCif(int $cif): self
    {
        $this->cif = $cif;

        return $this;
    }

    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    public function setCompanyName(string $companyName): self
    {
        $this->companyName = $companyName;

        return $this;
    }

    public function getBillingAddress(): ?string
    {
        return $this->billingAddress;
    }

    public function setBillingAddress(string $billingAddress): self
    {
        $this->billingAddress = $billingAddress;

        return $this;
    }

    public function getPhone(): ?int
    {
        return $this->phone;
    }

    public function setPhone(int $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getMobile(): ?int
    {
        return $this->mobile;
    }

    public function setMobile(int $mobile): self
    {
        $this->mobile = $mobile;

        return $this;
    }
}
