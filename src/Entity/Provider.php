<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Traits\ClientProviderTrait;
use App\Validator\Constraints as AppAssert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

// @AppAssert\ProviderConstraint
/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\ProviderRepository")
 * @UniqueEntity("email", entityClass="App\Entity\User")
 */
class Provider extends User
{
    use ClientProviderTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $deliveryTime;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $startDatePlan;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $endDatePlan;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OpenHour", mappedBy="provider", orphanRemoval=true, cascade={"persist"})
     * @Assert\Valid
     */
    private $openHours;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProviderClient", mappedBy="provider", orphanRemoval=true)
     * @Assert\Valid
     */
    private $providerClients;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Mail", mappedBy="provider", orphanRemoval=true)
     * @Assert\Valid
     */
    private $mails;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Plan", inversedBy="provider")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull
     */
    private $plan;

    // Por ahora no valido la relacion * @Assert\Valid
    /**
     * @ORM\OneToOne(targetEntity="App\Entity\FormPlanEnterprise", inversedBy="provider", cascade={"persist", "remove"})
     */
    private $formPlanEnterprise;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default" : 0})
     */
    private $isPlanExtend = false;

    public function __toString()
    {
        return $this->companyName;
    }

    public function __construct()
    {
        $this->openHours = new ArrayCollection();
        $this->providerClients = new ArrayCollection();
        $this->mails = new ArrayCollection();
    }

    public function isExpired()
    {
        return (!is_null($this->endDatePlan))
            ? date('Ymd') > $this->endDatePlan->format('Ymd')
            : false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDeliveryTime(): ?string
    {
        return $this->deliveryTime;
    }

    public function setDeliveryTime(string $deliveryTime): self
    {
        $this->deliveryTime = $deliveryTime;

        return $this;
    }

    public function getStartDatePlan(): ?\DateTimeInterface
    {
        return $this->startDatePlan;
    }

    public function setStartDatePlan(?\DateTimeInterface $startDatePlan): self
    {
        $this->startDatePlan = $startDatePlan;

        return $this;
    }

    public function getEndDatePlan(): ?\DateTimeInterface
    {
        return $this->endDatePlan;
    }

    public function setEndDatePlan(?\DateTimeInterface $endDatePlan): self
    {
        $this->endDatePlan = $endDatePlan;

        return $this;
    }

    /**
     * @return Collection|OpenHour[]
     */
    public function getOpenHours(): Collection
    {
        return $this->openHours;
    }

    public function addOpenHour(OpenHour $openHour): self
    {
        if (!$this->openHours->contains($openHour)) {
            $this->openHours[] = $openHour;
            $openHour->setProvider($this);
        }

        return $this;
    }

    public function removeOpenHour(OpenHour $openHour): self
    {
        if ($this->openHours->contains($openHour)) {
            $this->openHours->removeElement($openHour);
            // set the owning side to null (unless already changed)
            if ($openHour->getProvider() === $this) {
                $openHour->setProvider(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ProviderClient[]
     */
    public function getProviderClients(): Collection
    {
        return $this->providerClients;
    }

    public function addProviderClient(ProviderClient $providerClient): self
    {
        if (!$this->providerClients->contains($providerClient)) {
            $this->providerClients[] = $providerClient;
            $providerClient->setProvider($this);
        }

        return $this;
    }

    public function removeProviderClient(ProviderClient $providerClient): self
    {
        if ($this->providerClients->contains($providerClient)) {
            $this->providerClients->removeElement($providerClient);
            // set the owning side to null (unless already changed)
            if ($providerClient->getProvider() === $this) {
                $providerClient->setProvider(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Mail[]
     */
    public function getMails(): Collection
    {
        return $this->mails;
    }

    public function addMail(Mail $mail): self
    {
        if (!$this->mails->contains($mail)) {
            $this->mails[] = $mail;
            $mail->setProvider($this);
        }

        return $this;
    }

    public function removeMail(Mail $mail): self
    {
        if ($this->mails->contains($mail)) {
            $this->mails->removeElement($mail);
            // set the owning side to null (unless already changed)
            if ($mail->getProvider() === $this) {
                $mail->setProvider(null);
            }
        }

        return $this;
    }

    public function getPlan(): ?Plan
    {
        return $this->plan;
    }

    public function setPlan(?Plan $plan): self
    {
        $this->plan = $plan;

        return $this;
    }

    public function getFormPlanEnterprise(): ?FormPlanEnterprise
    {
        return $this->formPlanEnterprise;
    }

    public function setFormPlanEnterprise(?FormPlanEnterprise $formPlanEnterprise): self
    {
        $this->formPlanEnterprise = $formPlanEnterprise;

        return $this;
    }

    public function getIsPlanExtend(): ?bool
    {
        return $this->isPlanExtend;
    }

    public function setIsPlanExtend(bool $isPlanExtend): self
    {
        $this->isPlanExtend = $isPlanExtend;

        return $this;
    }
}
