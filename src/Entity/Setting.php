<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\SettingRepository")
 */
class Setting
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Url(
     *    relativeProtocol = true
     * )
     */
    private $emailService;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $versionAppAndroid;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $quantityEmailWeekly;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $maitenanceMode;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Country", mappedBy="setting")
     * @Assert\Valid
     */
    private $countries;

    public function __construct()
    {
        $this->countries = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmailService(): ?string
    {
        return $this->emailService;
    }

    public function setEmailService(string $emailService): self
    {
        $this->emailService = $emailService;

        return $this;
    }

    public function getVersionAppAndroid(): ?string
    {
        return $this->versionAppAndroid;
    }

    public function setVersionAppAndroid(?string $versionAppAndroid): self
    {
        $this->versionAppAndroid = $versionAppAndroid;

        return $this;
    }

    public function getQuantityEmailWeekly(): ?int
    {
        return $this->quantityEmailWeekly;
    }

    public function setQuantityEmailWeekly(?int $quantityEmailWeekly): self
    {
        $this->quantityEmailWeekly = $quantityEmailWeekly;

        return $this;
    }

    public function getMaitenanceMode(): ?bool
    {
        return $this->maitenanceMode;
    }

    public function setMaitenanceMode(?bool $maitenanceMode): self
    {
        $this->maitenanceMode = $maitenanceMode;

        return $this;
    }

    /**
     * @return Collection|Country[]
     */
    public function getCountries(): Collection
    {
        return $this->countries;
    }

    public function addCountry(Country $country): self
    {
        if (!$this->countries->contains($country)) {
            $this->countries[] = $country;
            $country->setSetting($this);
        }

        return $this;
    }

    public function removeCountry(Country $country): self
    {
        if ($this->countries->contains($country)) {
            $this->countries->removeElement($country);
            // set the owning side to null (unless already changed)
            if ($country->getSetting() === $this) {
                $country->setSetting(null);
            }
        }

        return $this;
    }
}
