<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\PlanRepository")
 */
class Plan
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $extendDays;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Provider", mappedBy="plan")
     * @Assert\Valid
     */
    private $provider;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotNull
     */
    private $numberUsers;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotNull
     */
    private $numberReferences;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default" : 0})
     */
    private $completeFormPlanEnterprise = false;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotNull
     */
    private $enableDay;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $color;

    public function __construct()
    {
        $this->provider = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getExtendDays(): ?int
    {
        return $this->extendDays;
    }

    public function setExtendDays(?int $extendDays): self
    {
        $this->extendDays = $extendDays;

        return $this;
    }

    /**
     * @return Collection|Provider[]
     */
    public function getProvider(): Collection
    {
        return $this->provider;
    }

    public function addProvider(Provider $provider): self
    {
        if (!$this->provider->contains($provider)) {
            $this->provider[] = $provider;
            $provider->setPlan($this);
        }

        return $this;
    }

    public function removeProvider(Provider $provider): self
    {
        if ($this->provider->contains($provider)) {
            $this->provider->removeElement($provider);
            // set the owning side to null (unless already changed)
            if ($provider->getPlan() === $this) {
                $provider->setPlan(null);
            }
        }

        return $this;
    }

    public function getCompleteFormPlanEnterprise(): ?bool
    {
        return $this->completeFormPlanEnterprise;
    }

    public function setCompleteFormPlanEnterprise(bool $completeFormPlanEnterprise): self
    {
        $this->completeFormPlanEnterprise = $completeFormPlanEnterprise;

        return $this;
    }

    public function getEnableDay(): ?int
    {
        return $this->enableDay;
    }

    public function setEnableDay(int $enableDay): self
    {
        $this->enableDay = $enableDay;

        return $this;
    }

    public function getNumberUsers(): ?int
    {
        return $this->numberUsers;
    }

    public function setNumberUsers(int $numberUsers): self
    {
        $this->numberUsers = $numberUsers;

        return $this;
    }

    public function getNumberReferences(): ?int
    {
        return $this->numberReferences;
    }

    public function setNumberReferences(int $numberReferences): self
    {
        $this->numberReferences = $numberReferences;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }
}
