<?php

namespace App\Security;

use App\Entity\Provider as Provider;
use App\Entity\User as AppUser;
use App\Exception\AccountDeletedException;
use Symfony\Component\Security\Core\Exception\DisabledException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserChecker implements UserCheckerInterface
{
    public function checkPreAuth(UserInterface $user)
    {
        if (!$user instanceof AppUser) {
            return;
        }

        // user is deleted, show a generic Account Not Found message.
        // if ($user->isDeleted()) {
        //     throw new AccountDeletedException();
        // }
    }

    public function checkPostAuth(UserInterface $user)
    {
        if ($user instanceof Provider) {
            if (!$user->getEnabled() || $user->isExpired()) {
                throw new DisabledException();
            }
        } elseif ($user instanceof AppUser) {
            if (!$user->getEnabled()) {
                throw new DisabledException();
            }
        } else {
            return;
        }
    }
}
