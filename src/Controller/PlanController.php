<?php

namespace App\Controller;

use App\Service\PlanService;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class PlanController extends EasyAdminController
{


/**
     * Creates the form object used to create or edit the given entity.
     *
     * @param object $entity
     * @param array  $entityProperties
     * @param string $view
     *
     * @return FormInterface
     *
     * @throws \Exception
     */
    protected function createEntityForm($entity, array $entityProperties, $view)
    {   
     
        if (method_exists($this, $customMethodName = 'create'.$this->entity['name'].'EntityForm')) {
            $form = $this->{$customMethodName}($entity, $entityProperties, $view);
            if (!$form instanceof FormInterface) {
                throw new \UnexpectedValueException(sprintf('The "%s" method must return a FormInterface, "%s" given.', $customMethodName, \is_object($form) ? \get_class($form) : \gettype($form)));
            }

            return $form;
        }

        
        $formBuilder = $this->executeDynamicMethod('create<EntityName>EntityFormBuilder', [$entity, $view]);

        // para agregar el campo extend days si es plan de prueba
        if ($formBuilder->getData()->getName()=='Plan Prueba') {
           $formBuilder
                
                ->get('extendDays', null,
                [
                    'label' => 'plan_ea.form.extendDays'
                ]);
        }else{
            
            $formBuilder->remove('extendDays');
        }

        // para ocultar este campo si no es plan enterprise
        if ($formBuilder->getData()->getName()!='Plan Enterprise') {
            $formBuilder
                 ->remove('completeFormPlanEnterprise');
         }

    

        return $formBuilder->getForm();
    }
}