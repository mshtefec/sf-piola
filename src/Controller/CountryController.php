<?php

namespace App\Controller;

use App\Entity\Country;
use App\Entity\State;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\File;

class CountryController extends EasyAdminController
{
    /**
     * @Route(path="/import", name="import")
     */
    public function importAction(Request $request)
    {
        $id = $request->query->get('id');

        // creo un form file con el id del pais a cargar sus provincias
        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('import'))
            ->setMethod('POST')
            ->add('id_country', HiddenType::class, [
                'data' => $id,
            ])
            ->add('file', FileType::class, [
                'label' => 'Provincias y Ciudades (CVS/XLS)',
                'required' => true,
                'mapped' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'text/csv',
                            'application/vnd.ms-excel',
                        ],
                        'mimeTypesMessage' => 'Subé un archivo .xls válido.',
                    ]),
                ],
            ])
            ->add('subir', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-info btn-block',
                ],
            ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // obtengo la id del pais previamente guardado en un campo oculto del form
            $id_country = $form['id_country']->getData();

            $em = $this->getDoctrine()->getManager();

            $repository = $this->getDoctrine()->getRepository(Country::class);
            $repositoryStates = $this->getDoctrine()->getRepository(State::class);

            // obtengo el pais actual
            $country = $repository->find($id_country);

            $states = $repositoryStates->findAllByCountry($country);

            // borro toda la colección de provincias y estados de ese pais
            foreach ($states as $s) {
                $em->remove($s);
            }
            $em->flush();

            // obtengo el fichero por el form
            $file = $form['file']->getData();

            // creo una instancia de phpspreadsheet con formato xls.
            $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xls');
            $spreadsheet = $reader->load($file->getPathName());

            // obtengo la primera hoja activa
            $sheet = $spreadsheet->getActiveSheet();

            // recorro por fila el excel
            foreach ($sheet->getRowIterator() as $key => $row) {
                // si es la primera fila salteo por los encabesados
                if ($key > 1) {
                    // $cellIterator = $row->getCellIterator();
                    // $cellIterator->setIterateOnlyExistingCells(false);

                    $state = $sheet->getCell('A'.$key);

                    $entity = new State();
                    $entity->setCountry($country);
                    $entity->setName($state);

                    $em->persist($entity);
                    $em->flush();
                }
            }

            $this->addFlash('success', 'El archivo ha sido cargado correctamente');

            return $this->redirectToRoute('easyadmin', [
                'action' => 'list',
                'entity' => 'Country',
            ]);
        }

        return $this->render('admin/country/import.html.twig', [
            'import_form' => $form->createView(),
        ]);
    }

    /**
     * @Route(path="/export", name="export")
     */
    public function exportAction(Request $request)
    {
        $id_country = $request->query->get('id');

        $em = $this->getDoctrine()->getManager();

        $repository = $this->getDoctrine()->getRepository(Country::class);
        $country = $repository->find($id_country);

        $repositoryStates = $this->getDoctrine()->getRepository(State::class);
        $states = $repositoryStates->findAllByCountry($country);

        $extension = '.xls';
        $nameFile = $country->getName().'_states_'.time().$extension;

        $temp_file = tempnam(sys_get_temp_dir(), $nameFile);

        $spreadsheet = new Spreadsheet();
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xls');

        $sheet = $spreadsheet->getActiveSheet();
        $sheet->getCell('A1')->setValue('nombre del estado');

        $key = 2;
        foreach ($states as $state) {
            $stateName = $state->getName();
            $sheet->getCell('A'.$key++)->setValue($stateName);
        }

        $writer->save($temp_file);

        return $this->file($temp_file, $nameFile, ResponseHeaderBag::DISPOSITION_INLINE);
    }
}
