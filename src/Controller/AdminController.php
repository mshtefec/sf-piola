<?php

namespace App\Controller;

use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;


class AdminController extends EasyAdminController
{   
    /**
     * @Route("/admin", name="easyadmin")
     */
    public function index(Request $request)
    {
        return parent::indexAction($request);
    }

    
    /**
     * @Route("/dashboard", name="dashboard")
     */
    public function dashboard()
    {   
        return $this->render(   
            'admin/dashboard/dashboard.html.twig'
            );
    }

    /**
     * @Route("/notifications", name="notifications")
     */
    public function notifications()
    {   
        return $this->render(   
            'admin/dashboard/dashboard.html.twig'
            );
    }

    /**
     * @Route("/orders", name="orders")
     */
    public function orders()
    {   
        return $this->render(   
            'admin/orders/list.html.twig'
            );
    }


}