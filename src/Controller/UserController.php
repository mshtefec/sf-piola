<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\Provider;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;

class UserController extends EasyAdminController
{
    protected function editAction()
    {
        if ($this->request->attributes->get('easyadmin')['item'] instanceof Provider) {
            return $this->redirectToRoute('easyadmin', [
                'action' => 'edit',
                'id' => $this->request->query->get('id'),
                'entity' => 'Provider',
                'from' => 'User',
            ]);
        } elseif ($this->request->attributes->get('easyadmin')['item'] instanceof Client) {
            return $this->redirectToRoute('easyadmin', [
                'action' => 'edit',
                'id' => $this->request->query->get('id'),
                'entity' => 'Client',
                'from' => 'User',
            ]);
        } else {
            return parent::editAction();
        }
    }
}
