<?php

namespace App\Controller;

use App\Entity\Provider;
use App\Entity\User;
use App\Form\ProviderType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    public $mailer;

    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/register", name="app_register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $user = new Provider();
        $form = $this->createForm(ProviderType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $password = $passwordEncoder->encodePassword($user, $user->getPassword());
            $user->setPassword($password);
            $user->setEnabled(false);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('app_login');
        }

        return $this->render('security/register.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/reset", name="app_reset_pass")
     */
    public function reset(Request $request, $domain_host)
    {
        $form = $this->createFormBuilder()
            ->add('email', EmailType::class, [
                'attr' => [
                    'placeholder' => 'Email',
                    'class' => 'form-control form-control-lg',
                ],
            ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $email = $form['email']->getData();

            $repository = $this->getDoctrine()->getRepository(User::class);
            $user = $repository->findOneByEmail($email);

            if ($user) {
                $random = random_bytes(10);
                $randomToken = bin2hex($random);

                $user->setTokenPassword($randomToken);

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                $entityManager->flush();

                $link = $domain_host.'/resetconfirmation/'.$randomToken;

                $this->sendReset($link, $email);

                $this->addFlash('warning', 'Se envio un email para restablecer la contraseña.');

                return $this->redirectToRoute('app_login');
            }
            $this->addFlash('warning', 'El correo no existe.');

            return $this->redirectToRoute('app_reset_pass');
        }

        return $this->render('security/reset.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/resetconfirmation/{token}", name="app_reset_confirmation")
     */
    public function resetConfirmation($token, Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $form = $this->createFormBuilder()
            ->add('token', HiddenType::class, [
                'data' => $token,
            ])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_options' => [
                    'attr' => [
                        'placeholder' => 'password',
                        'class' => 'form-control form-control-lg',
                    ],
                ],
                'second_options' => [
                    'attr' => [
                        'placeholder' => 'password_confirmation',
                        'class' => 'form-control form-control-lg',
                    ],
                ],
            ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $token = $form['token']->getData();
            $newPassword = $form['password']->getData();
            $repository = $this->getDoctrine()->getRepository(User::class);
            $user = $repository->findOneByTokenPassword($token);

            $password = $passwordEncoder->encodePassword($user, $newPassword);
            $user->setPassword($password);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash('success', 'Se ha restablecido tu contraseña.');

            return $this->redirectToRoute('app_login');
        }

        return $this->render('security/resetConfirmation.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function sendReset($url, $email)
    {
        $message = (new \Swift_Message('Reset Pass'))
        ->setFrom($this->getParameter('DELIVERY_MAIL'))
        ->setTo($email)
        ->setBody(
            $this->renderView(
                // templates/emails/registration.html.twig
                'email/reset.html.twig', [
                    'link' => $url,
                ]
            ),
            'text/html'
        );

        $this->mailer->send($message);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \Exception('This method can be blank - it will be intercepted by the logout key on your firewall');
    }
}
