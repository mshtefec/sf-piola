<?php

namespace App\Controller;

use App\Entity\Provider;
use App\Form\Provider\ProviderType;
use App\Service\PlanService;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class ProviderController extends EasyAdminController
{
    public $mailer;
    private $domain_host;

    public function __construct(\Swift_Mailer $mailer, $domain_host)
    {
        $this->mailer = $mailer;
        $this->domain_host = $domain_host;
    }

    public static function getSubscribedServices(): array
    {
        return array_merge(parent::getSubscribedServices(), [
                UserPasswordEncoderInterface::class,
                PlanService::class,
            ]);
    }

    protected function persistEntity($entity)
    {
        $this->get(PlanService::class)->configProviderPlan($entity);

        $entity->setRoles(['ROLE_PROVIDER']);
        $entity->setStatus(0);

        $password_resg = $this->request->request->all()['provider']['plainPassword']['first'];

        $entity->setPassword($this->get(UserPasswordEncoderInterface::class)->encodePassword(
            $entity,
            $this->request->request->all()['provider']['plainPassword']['first']
        ));

        //send invitation mail
        $this->sendInvitation($entity, $password_resg);

        parent::persistEntity($entity);
    }

    protected function updateEntity($entity)
    {
        $oldData = $this->em
            ->getUnitOfWork()
            ->getOriginalEntityData($entity);

        $planChange = $oldData['plan_id'] != $entity->getPlan()->getId();
        $planExtend = (!$oldData['isPlanExtend'] && $entity->getIsPlanExtend());

        $this->get(PlanService::class)->configProviderPlan($entity, $planChange, $planExtend);

        if ($this->request->request->all()['provider']['plainPassword']['first']) {
            $entity->setPassword($this->get(UserPasswordEncoderInterface::class)->encodePassword(
                $entity,
                $this->request->request->all()['provider']['plainPassword']['first']
            ));
        }

        parent::updateEntity($entity);
    }

    protected function redirectToReferrer()
    {
        if ('User' == $this->request->query->get('from', '')) {
            return $this->redirectToRoute('easyadmin', [
                'action' => 'list',
                'entity' => 'User',
            ]);
        }

        return parent::redirectToReferrer();
    }

    public function sendInvitation($entity, $password_resg)
    {
        $message = (new \Swift_Message('Invitación a Piola'))
        ->setFrom($this->getParameter('DELIVERY_MAIL'))
        ->setTo($entity->getEmail())
        ->setBody(
            $this->renderView('email/invitation.html.twig', [
                'link' => $this->domain_host,
                'entity' => $entity,
                'password' => $password_resg
            ]),
            'text/html'
        );

        $this->mailer->send($message);
    }

    /**
     * @Route(path="/information", name="provider_info")
     */
    public function infoAction()
    {
        $user = $this->getUser();
        $id = $user->getId();
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(Provider::class);
        $entity = $repository->find($id);

        return $this->render('admin/provider/info.html.twig', [
            'entity' => $entity,
        ]);
    }

    /**
     * @Route(path="/edit", name="edit")
     */
    protected function editAction()
    {
        $user = $this->getUser();
        $id = $user->getId();
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(Provider::class);
        $entity = $repository->find($id);

        $form = $this->createForm(ProviderType::class, $entity);

        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $em->persist($entity);
            $em->flush();

            return $this->redirectToRoute('app_login');
        }

        return $this->render('admin/provider/edit.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }
}
