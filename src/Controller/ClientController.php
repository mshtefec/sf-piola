<?php

namespace App\Controller;

use App\Entity\ProviderClient;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ClientController extends EasyAdminController
{
    public $mailer;
    private $domain_host;

    public function __construct(\Swift_Mailer $mailer, $domain_host)
    {
        $this->mailer = $mailer;
        $this->domain_host = $domain_host;
    }

    public static function getSubscribedServices(): array
    {
        return array_merge(parent::getSubscribedServices(), [UserPasswordEncoderInterface::class]);
    }

    protected function createListQueryBuilder($entityClass, $sortDirection, $sortField = null, $dqlFilter = null)
    {
        $user = $this->getUser();

        /* @var EntityManager */
        $em = $this->getDoctrine()->getManagerForClass($this->entity['class']);

        if ($this->isGranted('ROLE_ADMIN')) {
            $queryBuilder = $em->createQueryBuilder()
                ->select('entity')
                ->from($this->entity['class'], 'entity')
            ;
        } else {
            $queryBuilder = $em->createQueryBuilder()
                ->select('entity')
                ->from($this->entity['class'], 'entity')
                ->leftJoin('entity.providerClients', 'providerClients')
                ->where('providerClients.provider = :id')
                ->setParameter('id', $user->getId())
            ;
        }

        if (!empty($dqlFilter)) {
            $queryBuilder->andWhere($dqlFilter);
        }

        if (null !== $sortField) {
            $queryBuilder->orderBy('entity.'.$sortField, $sortDirection ?: 'DESC');
        }

        return $queryBuilder;
    }

    protected function persistEntity($entity)
    {
        $entity->setRoles(['ROLE_CLIENT']);

        $password_resg = $this->request->request->all()['client']['plainPassword']['first'];

        $entity->setPassword($this->get(UserPasswordEncoderInterface::class)->encodePassword(
            $entity,
            $this->request->request->all()['client']['plainPassword']['first']
        ));

        if ($this->isGranted('ROLE_PROVIDER')) {
            $provider = $this->getUser();
            $providerClient = new ProviderClient();
            $providerClient->setProvider($provider);
            $providerClient->setClient($entity);
            $providerClient->setDiscount(0);
            $this->em->persist($providerClient);
        }

        //send invitation mail

        $this->sendInvitation($entity, $password_resg);

        parent::persistEntity($entity);
    }

    protected function updateEntity($entity)
    {
        if ($this->request->request->all()['client']['plainPassword']['first']) {
            $entity->setPassword($this->get(UserPasswordEncoderInterface::class)->encodePassword(
                $entity,
                $this->request->request->all()['client']['plainPassword']['first']
            ));
        }

        parent::updateEntity($entity);
    }

    protected function redirectToReferrer()
    {
        if ('User' == $this->request->query->get('from', '')) {
            return $this->redirectToRoute('easyadmin', [
                'action' => 'list',
                'entity' => 'User',
            ]);
        }

        return parent::redirectToReferrer();
    }

    public function sendInvitation($entity, $password_resg)
    {
        $message = (new \Swift_Message('Invitación a Piola'))
        ->setFrom($this->getParameter('DELIVERY_MAIL'))
        ->setTo($entity->getEmail())
        ->setBody(
            $this->renderView('email/invitation.html.twig', [
                'link' => $this->domain_host,
                'entity' => $entity,
                'password' => $password_resg,
            ]),
            'text/html'
        );

        $this->mailer->send($message);
    }
}
