<?php

namespace App\Repository;

use App\Entity\FormPlanEnterprise;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method FormPlanEnterprise|null find($id, $lockMode = null, $lockVersion = null)
 * @method FormPlanEnterprise|null findOneBy(array $criteria, array $orderBy = null)
 * @method FormPlanEnterprise[]    findAll()
 * @method FormPlanEnterprise[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FormPlanEnterpriseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FormPlanEnterprise::class);
    }

    // /**
    //  * @return FormPlanEnterprise[] Returns an array of FormPlanEnterprise objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FormPlanEnterprise
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
