<?php

namespace App\Repository;

use App\Entity\ProviderClient;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ProviderClient|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProviderClient|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProviderClient[]    findAll()
 * @method ProviderClient[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProviderClientRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProviderClient::class);
    }

    // /**
    //  * @return ProviderClient[] Returns an array of ProviderClient objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProviderClient
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
