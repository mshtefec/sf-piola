var showformPlanEnterprise = function (show) {
    if(show) {
        $(".field-form_plan_enterprise").parent().show()
    } else {
        $(".field-form_plan_enterprise").parent().hide()
    }
}

$(':radio[name="provider[plan]"]').change(function() {
    showformPlanEnterprise($(this).data('completeformplanenterprise') === 1)
});

$(function() {
    showformPlanEnterprise(
        $(':radio[name="provider[plan]"]:checked')
            .data('completeformplanenterprise')
    )
})